package com.aezzata.tobiasfunke;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends ActionBarActivity {
	RawPlayer rp = new RawPlayer(this);
	private AdView adView;

	public void onRadioButtonClicked(View view) {
		// is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// check which radio button was checked
		switch (view.getId()) {
		case R.id.soundA:
			if (checked) {
				rp.setAndPlayMedia(R.raw.hazzah);
			}
			break;

		case R.id.soundB:
			if (checked) {
				rp.setAndPlayMedia(R.raw.blowhard);
			}
			break;

		case R.id.soundC:
			if (checked) {
				rp.setAndPlayMedia(R.raw.bluemyself);
			}
			break;

		case R.id.soundD:
			if (checked) {
				rp.setAndPlayMedia(R.raw.chuppy);
			}
			break;

		case R.id.soundE:
			if (checked) {
				rp.setAndPlayMedia(R.raw.dozens);
			}
			break;

		case R.id.soundF:
			if (checked) {
				rp.setAndPlayMedia(R.raw.maninside);

			}
			break;

		case R.id.soundG:
			if (checked) {
				rp.setAndPlayMedia(R.raw.arrow);
			}
			break;

		case R.id.soundH:
			if (checked) {
				rp.setAndPlayMedia(R.raw.premature);
			}
			break;

		}
	}

	public void playSound(View view) {
		rp.playMediaFile();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		AdView adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);		

		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
